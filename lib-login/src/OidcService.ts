import {
    UserManagerSettings,
    Log,
    User,
    UserLoadedCallback,
    SilentRenewErrorCallback,
    UserManager,
  } from 'oidc-client-ts';
  import jwtDecode from 'jwt-decode';
  
  const BASE_URL = 'http://localhost:4100/';
  const BASE_PATH = 'account/';
  const PATH_FOR_AUTHWITHREDIRECT = 'authWithRedirect';
  const PATH_FOR_AUTHWITHPOPUP = 'authWithPopup';
  const IDP = 'https://lemur-12.cloud-iam.com/auth/realms/j/';
  const CLIENT_ID = 'ufe4';
  const SCOPE = 'openid';
  const REDIRECT_URI = `${BASE_URL}${BASE_PATH}${PATH_FOR_AUTHWITHREDIRECT}`;
  const POST_LOGOUT_REDIRECT_URI = `${BASE_URL}`;
  
  export class OidcService {
    private userManager: UserManager;
  
    constructor() {
      Log.setLogger(console);
      Log.setLevel(Log.DEBUG);
      const userManagerSettings: UserManagerSettings = {
        authority: IDP,
        client_id: CLIENT_ID,
        scope: SCOPE,
        redirect_uri: REDIRECT_URI,
        post_logout_redirect_uri: POST_LOGOUT_REDIRECT_URI,
      };
      this.userManager = new UserManager(userManagerSettings);
      this.userManager.events.addUserLoaded((user: User) => {
        console.log('Logged in user');
        console.log(user);
      });
      this.userManager.events.addSilentRenewError((error: Error) => {
        console.error(`Error refreshing tokens ${error}`);
      });
    }
  
    public async loginAuthorizationCodeFlowWithPkce(_backurl: string): Promise<void> {
      console.log(_backurl);
      await this.loginAuthorizationCodeFlowWithPkceRedirect(_backurl);
      //await this.loginAuthorizationCodeFlowWithPkcePopup();
    }

    public async loginAuthorizationCodeFlowWithPkceRedirect(backurl: string): Promise<void> {
      const encodedBackurl = encodeURIComponent(backurl);
      await this.userManager.signinRedirect({
        redirect_uri: `${BASE_URL}${BASE_PATH}${PATH_FOR_AUTHWITHREDIRECT}?backurl=${encodedBackurl}`,
      });
    }
  
    public async loginAuthorizationCodeFlowWithPkceRedirectCallback(): Promise<void> {
      await this.userManager.signinRedirectCallback();
    }
  
    public async loginAuthorizationCodeFlowWithPkcePopup(): Promise<void> {
      await this.userManager.signinPopup({
        redirect_uri: `${BASE_URL}${BASE_PATH}${PATH_FOR_AUTHWITHPOPUP}`,
      });
    }
  
    public async loginAuthorizationCodeFlowWithPkcePopupCallback(): Promise<void> {
      await this.userManager.signinPopupCallback();
    }
  
    public async logout(): Promise<void> {
      await this.userManager.signoutRedirect();
    }
  
    public async getUser(): Promise<User | null> {
      return this.userManager.getUser();
    }
  
    public addUserLoadedCallback(userLoadedCallback: UserLoadedCallback): void {
      this.userManager.events.addUserLoaded(userLoadedCallback);
    }
  
    public addSilentRenewErrorCallback(silentRenewErrorCallback: SilentRenewErrorCallback): void {
      this.userManager.events.addSilentRenewError(silentRenewErrorCallback);
    }
  }
  
  export const oidcService: OidcService = new OidcService();
  
  export function jwt2String(jwt: string | undefined): string {
    try {
      if (jwt === undefined) {
        return '';
      }
      const header = jwtDecode(jwt, {
        header: true,
      });
      const headerJson = JSON.stringify(header, null, 2);
      const payload = jwtDecode(jwt);
      const payloadJson = JSON.stringify(payload, null, 2);
      return `${headerJson}\n${payloadJson}`;
    } catch (e) {
      console.error(e);
      return 'Not a JWT token?';
    }
  }
