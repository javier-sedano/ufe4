const express = require('express');
const httpProxy = require('http-proxy');

const PORT = 4100;

const maps = [
    {
        path: '/account*',
        target: 'http://127.0.0.1:4104',
    },
    {
        path: '/contact*',
        target: 'http://127.0.0.1:4102',
    },
    {
        path: '/products*',
        target: 'http://127.0.0.1:4103',
    },
    {
        path: '/*',
        target: 'http://127.0.0.1:4101',
    },
];

const app = express();
const proxy = httpProxy.createProxyServer({});

maps.forEach((map) => {
    app.get(map.path, (req, res) => {
        console.log(`${req.url} -> ${map.target}`);
        proxy.web(req, res, {
            target: map.target,
            changeOrigin: true,
        });
    });
});

proxy.on('error', function (err, req, res) {
    console.log(err);
    res.writeHead(500, {
        'Content-Type': 'text/plain'
    });
    res.end(`${err}`);
});

app.listen(PORT);
console.log(`**************************************\n* Listening on http://localhost:${PORT} *\n**************************************`);
